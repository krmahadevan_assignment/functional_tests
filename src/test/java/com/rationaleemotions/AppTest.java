package com.rationaleemotions;

import com.rationaleemotions.helpers.Credentials;
import com.rationaleemotions.helpers.DBHelper;
import com.rationaleemotions.helpers.JdbcUrl;
import com.rationaleemotions.pojos.Host;

public class AppTest {

    public static void main(String[] args) {
        JdbcUrl url = new JdbcUrl(new Host("localhost", 5432), "assessment_db");
        DBHelper helper = new DBHelper(url, new Credentials("postgres", "postgres"));
        System.err.println(helper.latestEntryInLedger());
    }
}
