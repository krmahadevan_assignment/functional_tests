package com.rationaleemotions.tests;

import com.rationaleemotions.annotations.TestScenario;
import com.rationaleemotions.helpers.ManagerHelper;
import com.rationaleemotions.pojos.JobScheduleLedger;
import com.rationaleemotions.pojos.JobStatus;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class NegativeTests extends AbstractTest {

    @TestScenario(
            description = "Manager has no worker nodes and a job scheduling is requested.",
            outcome = "The job scheduling should get aborted with a proper reason code in the ledger table."
    )
    @Test
    public void ensureJobSchedulingAbortsWhenNoWorkersFound() {
        workerProxy().heartbeat().block();
        // Let's wait for the unhooking to actually happen
        Awaitility.await()
                .atMost(Duration.ofMinutes(2))
                .pollInterval(Duration.ofSeconds(5))
                .until(() -> manager().isNodePoolEmpty());
        //Generate test data for ingestion
        manager().generateRecords(10);
        Optional<ManagerHelper.JobResponse> ingestionResult = manager().scheduleIngestion();
        assertThat(ingestionResult.isPresent()).isTrue();
        long id = ingestionResult.get().jobId();
        Optional<JobScheduleLedger> latest = pollForJobToMatch(id, JobStatus.Aborted);
        JobScheduleLedger ledger = latest.get();
        assertThat(ledger)
                .withFailMessage("Journal entry missing for aborted ingestion")
                .isNotNull();
        JobStatus status = JobStatus.fromString(ledger.getJobStatus());
        assertThat(status)
                .withFailMessage(ledger.getId() + " Job status is NOT Aborted")
                .isEqualTo(JobStatus.Aborted);
        assertThat(ledger.getReason())
                .withFailMessage("No Failure reason found")
                .isNotNull();
    }

    @TestScenario(
            description = "There's no data to be ingested. Manager wakes up and tries to schedule an ingestion.",
            outcome = "Ingestion attempt should fail and there should be a log in the journal about this failed attempt."
    )
    @Test
    public void ensureJobSchedulingAbortsWhenNoIngestionDataAvailable() {
        db().purgeAllIngestionData();
        Optional<JobScheduleLedger> baseline = db().latestEntryInLedger();
        manager().scheduleIngestion();
        sleepQuietlyForSeconds(10);
        Optional<JobScheduleLedger> latest = db().latestEntryInLedger();
        if (latest.isEmpty()) {
            //This can happen when the db does not have any records.
            assertThat(latest.isEmpty())
                    .withFailMessage("There should not have been any data in the journal")
                    .isTrue();
        } else {
            //This means that there is already data that exists for previous ingestions.
            //We need to ensure that no new records got created.
            assertThat(baseline.get().getId())
                    .withFailMessage("There should not have been any data in the journal")
                    .isEqualTo(latest.get().getId());
        }
    }

    @TestScenario(
            description = "Manager schedules a job and worker starts ingesting it. " +
                    "Now block job status. After about 20 seconds, try scheduling another ingestion.",
            outcome = "Manager should fail the inflight job. " +
                    "The next schedule should be rejected by the worker and " +
                    "it should NOT have any entries in the ledger"
    )
    @Test
    public void ensureJobGetsRejectedByNodeWhenManagerMarksItsInflightJobAsFailed() {
        try {
            manager().generateRecords(50);
            manager().scheduleIngestion();
            Optional<JobScheduleLedger> baseLineEntry = db().latestEntryInLedger();
            assertThat(baseLineEntry).isPresent();
            workerProxy().jobStatus().block();
            pollForJobToMatch(baseLineEntry.get().getId(), JobStatus.Failed);
            workerProxy().jobStatus().unblock();
            //At this point the node is still processing our job, but according to the manager
            //the node has been marked free.
            //So let's try to schedule a job. That job should be rejected by the worker
            worker().registerWorker();
            manager().generateRecords(5);
            manager().scheduleIngestion();
            Optional<JobScheduleLedger> latestEntry = db().latestEntryInLedger();
            assertThat(latestEntry).isPresent();
            assertThat(latestEntry.get().getId())
//                    .withFailMessage("There should not have been any new data in the ledger")
                    .isEqualTo(baseLineEntry.get().getId());
            //By the time we are here, the node may still have been processing our work, because we gave
            //30 records, Lets ensure that exit ONLY after the worker becomes free, else the next test
            //case fail, since we have ONLY 1 node.

            Awaitility.await()
                    .atMost(Duration.ofMinutes(2))
                    .pollInterval(Duration.ofSeconds(5))
                    .until(() -> {
                        boolean result = worker().free();
                        if (!result) {
                            log.info("Worker is still processing the ingestion request.");
                        }
                        return result;
                    }, result -> result);

        } finally {
            workerProxy().jobStatus().unblock();
        }
    }

    @TestScenario(
            description = "Manager schedules an ingestion. " +
                    "Worker now sends duplicate final status (first failed and then passed)",
            outcome = "The ingestion job should be failed."
    )
    @Test
    public void ensureIngestionStatusIsNotOverridable() {
        manager().generateRecords(50);
        Optional<ManagerHelper.JobResponse> ingestionResult = manager().scheduleIngestion();
        assertThat(ingestionResult.isPresent()).isTrue();
        long id = ingestionResult.get().jobId();
        //Ingestion will take 50 * 1 = 50 seconds to complete.
        sleepQuietlyForSeconds(20);
        //We should be midway in the ingestion.
        //Lets start sending fake status to manager
        workerProxy().jobStatus().fudgeFailures().enable();
        //Hopefully by now, the worker should have sent a failure status.
        sleepQuietlyForSeconds(12);
        //Lets revert the fudging so that the worker starts sending actual status.
        workerProxy().jobStatus().fudgeFailures().disable();


        Awaitility.await()
                .atMost(Duration.ofMinutes(2))
                .pollInterval(Duration.ofSeconds(5))
                .until(() -> worker().free());

        Optional<JobScheduleLedger> baseLineEntry = db().getLedgerEntryForJobId(id);
        assertThat(baseLineEntry).isPresent();
        log.info("Base line entry: {}", baseLineEntry.get());
        JobStatus status = JobStatus.fromString(baseLineEntry.get().getJobStatus());
        assertThat(status)
                .withFailMessage(baseLineEntry.get().getId() + " Job status should have been failed")
                .isEqualTo(JobStatus.Failed);
        assertThat(baseLineEntry.get().getReason())
                .withFailMessage("There should not be any reason, when worker marks a job as failed")
                .isNull();
    }

}
