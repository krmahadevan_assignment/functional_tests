package com.rationaleemotions.tests;

import com.google.gson.JsonParser;
import com.rationaleemotions.annotations.TestScenario;
import com.rationaleemotions.helpers.ManagerHelper;
import com.rationaleemotions.pojos.JobScheduleLedger;
import com.rationaleemotions.pojos.JobStatus;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class FunctionalTests extends AbstractTest {

    @TestScenario(
            description = "Manager schedules a job and worker starts ingesting it. Now block heartbeat.",
            outcome = "Ensure node gets removed but job gets completed by worker."
    )
    @Test
    public void ensureJobCompletionHappensEvenWhenNodeProcessingDataGetsDeregisteredDueToMissingHeartBeats() {
        manager().generateRecords(50);
        Optional<ManagerHelper.JobResponse> ingestionResult = manager().scheduleIngestion();
        assertThat(ingestionResult).isPresent();
        long id = ingestionResult.get().jobId();
        sleepQuietlyForSeconds(5);
        workerProxy().heartbeat().block();

        Optional<JobScheduleLedger> latestEntry = pollForJobToMatch(id, JobStatus.Success);
        assertThat(latestEntry)
                .withFailMessage("We should have found an entry in the ledger")
                .isPresent();
        JobScheduleLedger latest = latestEntry.get();
        JobStatus status = JobStatus.fromString(latest.getJobStatus());
        assertThat(status)
                .withFailMessage(latest.getId() + " should have status as success")
                .isEqualTo(JobStatus.Success);

        //Heart beats are blocked. So node should have been removed.
        Awaitility.await()
                .atMost(Duration.ofMinutes(5))
                .pollInterval(Duration.ofSeconds(5))
                .until(() -> manager().isNodePoolEmpty());

        // Ensure we cannot submit any more new jobs
        manager().generateRecords(10);
        ingestionResult = manager().scheduleIngestion();
        assertThat(ingestionResult).isPresent();
        id = ingestionResult.get().jobId();
        latestEntry = pollForJobToMatch(id, JobStatus.Aborted);
        assertThat(latestEntry).isPresent();
        latest = latestEntry.get();
        assertThat(latest)
                .withFailMessage("Journal entry missing for aborted ingestion")
                .isNotNull();
        status = JobStatus.fromString(latest.getJobStatus());
        assertThat(status)
                .withFailMessage("Job status is NOT Aborted")
                .isEqualTo(JobStatus.Aborted);
        assertThat(latest.getReason())
                .withFailMessage("No Failure reason found")
                .isNotNull();
    }

    @TestScenario(
            description = "Manager schedules an ingestion and worker successfully completes the ingestion.",
            outcome = "The table should have a successful value for the file location in s3. " +
                    "Ensure that the object resides in s3 and is available for use."
    )
    @Test
    public void ensureIngestionHappensAndFileAvailableInS3() {
        manager().generateRecords(20);
        Optional<ManagerHelper.JobResponse> ingestionResult = manager().scheduleIngestion();
        assertThat(ingestionResult).isPresent();
        long jobId = ingestionResult.get().jobId();

        Optional<JobScheduleLedger> latestEntry = pollForJobToMatch(jobId, 5, JobStatus.Success);
        assertThat(latestEntry).isPresent();
        String key = "JobId" + jobId;
        String result = aws().fileContents(bucketName(), key);
        int count = JsonParser.parseString(result).getAsJsonArray().size();
        assertThat(count)
                .withFailMessage("Number of records should have matched between DB and S3")
                .isEqualTo(latestEntry.get().getRecords());
    }

    @TestScenario(
            description = "Manager schedules an ingestion. Worker starts processing it. " +
                    "Block job status permanently (Simulating a stale job)",
            outcome = "After a staleness period is over, the inflight job should have gotten marked as " +
                    "failed (due to it being stale, with a proper reason code)"
    )
    @Test
    public void ensureStaleIngestionJobsAreFailed() {
        manager().generateRecords(20);
        Optional<ManagerHelper.JobResponse> ingestionResult = manager().scheduleIngestion();
        assertThat(ingestionResult).isPresent();
        long jobId = ingestionResult.get().jobId();
        workerProxy().jobStatus().block();
        Optional<JobScheduleLedger> latestEntry = pollForJobToMatch(jobId, 5, JobStatus.Failed);
        assertThat(latestEntry).isPresent();
        assertThat(latestEntry.get().getReason())
                .withFailMessage("No Failure reason found")
                .isNotNull();
    }
}
