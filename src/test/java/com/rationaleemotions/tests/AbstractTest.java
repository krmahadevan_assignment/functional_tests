package com.rationaleemotions.tests;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.rationaleemotions.helpers.*;
import com.rationaleemotions.listeners.TestContextSetupListener;
import com.rationaleemotions.pojos.Host;
import com.rationaleemotions.pojos.JobScheduleLedger;
import com.rationaleemotions.pojos.JobStatus;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.testng.ISuite;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
@Slf4j
public abstract class AbstractTest {

    private WorkerProxyHelper workerProxy;
    private ManagerHelper managerHelper;
    private WorkerHelper workerHelper;
    private DBHelper dbHelper;
    private AwsHelper awsHelper;
    private String bucketName;

    @BeforeMethod
    public void setup() {

        //There could be some in-flight jobs that a node may have been processing from the
        //previous iteration. So wait for them to complete.
        Awaitility.await()
                .atMost(Duration.ofMinutes(5))
                .pollInterval(Duration.ofSeconds(10))
                .until(() -> worker().free());
        ensureWorkerAvailability();
    }

    @AfterMethod
    public void cleanup() {
        ensureWorkerAvailability();
    }

    public AwsHelper aws() {
        if (awsHelper == null) {
            awsHelper = new AwsHelper(endpointConfiguration(), awsCredentials());
        }
        return awsHelper;
    }

    public ManagerHelper manager() {
        if (managerHelper == null) {
            managerHelper = new ManagerHelper(managerHostInfo());
        }
        return managerHelper;
    }

    public String bucketName() {
        if (bucketName == null) {
            bucketName = getBean(TestContextSetupListener.BUCKET_NAME, "AWS Bucket name");
        }
        return bucketName;
    }

    public WorkerHelper worker() {
        if (workerHelper == null) {
            workerHelper = new WorkerHelper(workerHostInfo());
        }
        return workerHelper;
    }

    public WorkerProxyHelper workerProxy() {
        if (workerProxy == null) {
            workerProxy = new WorkerProxyHelper(workerProxyInfo());
        }
        return workerProxy;
    }

    public DBHelper db() {
        if (dbHelper == null) {
            Host host = getBean(TestContextSetupListener.DB_HOST, "DB Host");
            String dbName = asString(TestContextSetupListener.DB_NAME, "DB Name");
            JdbcUrl url = new JdbcUrl(host, dbName);
            Credentials credentials = getBean(TestContextSetupListener.DB_CREDENTIALS, "DB Credentials");
            dbHelper = new DBHelper(url, credentials);
        }
        return dbHelper;
    }

    public void sleepQuietlyForSeconds(long seconds) {
        try {
            log.info("{}() is sleeping for {} seconds", currentlyRunningTestCase(), seconds);
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public Optional<JobScheduleLedger> pollForIngestionToComplete() {
        String tc = currentlyRunningTestCase();
        return Awaitility.await()
                .atMost(Duration.ofMinutes(2))
                .pollInterval(Duration.ofSeconds(30))
                .until(() -> db().latestEntryInLedger(), value -> {
                            if (value.isEmpty()) {
                                return false;
                            }
                            JobScheduleLedger current = value.get();
                            JobStatus currentStatus = JobStatus.fromString(current.getJobStatus());
                            log.info("[{}] Job id {} Current status: {}", tc, current.getId(), currentStatus);
                            return currentStatus == JobStatus.Success;
                        }
                );
    }

    public Optional<JobScheduleLedger> pollForJobWithId(long jobId) {
        String tc = currentlyRunningTestCase();
        return Awaitility.await()
                .atMost(Duration.ofMinutes(2))
                .pollInterval(Duration.ofSeconds(30))
                .until(() -> db().getLedgerEntryForJobId(jobId), Optional::isPresent);
    }

    public Optional<JobScheduleLedger> pollForJobToMatch(long jobId, JobStatus status) {
        return pollForJobToMatch(jobId, 2, status);
    }

    public Optional<JobScheduleLedger> pollForJobToMatch(long jobId, long minutes, JobStatus status) {
        String tc = currentlyRunningTestCase();
        return Awaitility.await()
                .atMost(Duration.ofMinutes(2))
                .pollInterval(Duration.ofSeconds(30))
                .until(() -> db().getLedgerEntryForJobId(jobId), value -> {
                            if (value.isEmpty()) {
                                return false;
                            }
                            JobScheduleLedger current = value.get();
                            JobStatus currentStatus = JobStatus.fromString(current.getJobStatus());
                            log.info("[{}] Job id {} Current status: {}", tc, jobId, currentStatus);
                            return currentStatus == status;
                        }
                );
    }

    private void ensureWorkerAvailability() {
        //Enable heartbeats so that node works as expected
        workerProxy().heartbeat().unblock();
        //Enable job status so that node works as expected
        workerProxy().jobStatus().unblock();
        //Ensure that failures are not simulated
        workerProxy().jobStatus().fudgeFailures().disable();
        //Explicitly register the worker once so that tests don't have unexpected behaviours
        //due to previous tests changing the system state
        worker().registerWorker();
    }

    private AwsClientBuilder.EndpointConfiguration endpointConfiguration() {
        return getBean(TestContextSetupListener.S3_ENDPOINT, "S3 EndPoint");
    }

    private AWSCredentials awsCredentials() {
        return getBean(TestContextSetupListener.AWS_CREDENTIALS, "AWS Credentials");
    }

    private Host managerHostInfo() {
        return getBean(TestContextSetupListener.MANAGER_HOST, "Manager Process");
    }


    private Host managerProxyInfo() {
        return getBean(TestContextSetupListener.MANAGER_PROXY, "Manager Proxy Process");
    }

    private Host workerHostInfo() {
        return getBean(TestContextSetupListener.WORKER_HOST, "Worker Process");
    }

    private Host workerProxyInfo() {
        return getBean(TestContextSetupListener.WORKER_PROXY, "Worker Proxy Process");
    }

    @SuppressWarnings("unchecked")
    private <T> T getBean(String attributeName, String errorMsgSuffix) {
        return (T) Objects.requireNonNull(
                suite().getAttribute(attributeName),
                "Could NOT find valid information for " + errorMsgSuffix
        );
    }

    private static String currentlyRunningTestCase() {
        return Reporter.getCurrentTestResult().getMethod().getMethodName();
    }

    private static String asString(String attributeName, String errorMsgSuffix) {
        ISuite suite = suite();
        return Objects.requireNonNull(suite().getAttribute(attributeName),
                "Could NOT find valid information for " + errorMsgSuffix).toString();
    }

    private static ISuite suite() {
        return Reporter.getCurrentTestResult().getTestContext().getSuite();
    }
}
