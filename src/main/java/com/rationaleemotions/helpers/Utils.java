package com.rationaleemotions.helpers;

import com.rationaleemotions.pojos.Host;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Preconditions;

@UtilityClass
@Slf4j
class Utils {

    static RequestSpecification spec(Host machine) {
        String uri = String.format("http://%s:%d", machine.host(), machine.port());
        return RestAssured.given().baseUri(uri).log().ifValidationFails()
                .contentType(ContentType.JSON);
    }

    public static void ensureOk(Response response, String errorTemplate) {
        Preconditions.checkState(ok(response), errorTemplate);

    }

    private static boolean ok(Response response) {
        return response.statusCode() == 200;
    }

}
