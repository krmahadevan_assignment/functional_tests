package com.rationaleemotions.helpers;

import com.rationaleemotions.pojos.Host;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import static com.rationaleemotions.helpers.Utils.ensureOk;
import static com.rationaleemotions.helpers.Utils.spec;

@SuppressWarnings("unused")
@Slf4j
public record WorkerProxyHelper(Host machine) {

    public Delays delay() {
        return new DelaysImpl(machine);
    }

    public HeartBeats heartbeat() {
        return new HeartBeatsImpl(machine);
    }

    public JobStatus jobStatus() {
        return new JobStatusImpl(machine);
    }

    public interface JobStatus {

        Failures fudgeFailures();

        void block();

        void unblock();
    }

    public interface Failures {
        void enable();

        void disable();
    }

    record FailuresImpl(Host machine) implements Failures {

        @Override
        public void enable() {
            Response response = spec(machine)
                    .param("mark-failed", true)
                    .get("/api/v1/job-status");
            ensureOk(response, "Could NOT enable Job Status fudging");
            log.info("Job Status fudging enabled");

        }

        @Override
        public void disable() {
            Response response = spec(machine).get("/api/v1/job-status");
            ensureOk(response, "Could NOT enable Job Status fudging");
            log.info("Job Status fudging disabled");
        }
    }

    record JobStatusImpl(Host machine) implements JobStatus {

        @Override
        public Failures fudgeFailures() {
            return new FailuresImpl(machine);
        }

        @Override
        public void block() {
            Response response = spec(machine)
                    .queryParam("block", true).get("/api/v1/job-block");
            ensureOk(response, "Could NOT block Job Status");
            log.info("Job Status blocked");
        }

        @Override
        public void unblock() {
            Response response = spec(machine).get("/api/v1/job-block");
            ensureOk(response, "Could NOT unblock heart beats");
            log.info("Job Status unblocked");
        }
    }

    public interface HeartBeats {
        void block();

        void unblock();
    }

    record HeartBeatsImpl(Host machine) implements HeartBeats {

        @Override
        public void block() {
            Response response = spec(machine)
                    .queryParam("block", true).get("/api/v1/heartbeat");
            ensureOk(response, "Could NOT block HeartBeats");
            log.info("HeartBeats blocked");
        }

        @Override
        public void unblock() {
            Response response = spec(machine).get("/api/v1/heartbeat");
            ensureOk(response, "Could NOT unblock heart beats");
            log.info("HeartBeats unblocked");
        }
    }

    public interface Delays {
        void enable(long value, DurationUnit unit);

        void disable();
    }

    record DelaysImpl(Host machine) implements Delays {

        @Override
        public void enable(long value, DurationUnit unit) {
            Response response = spec(machine)
                    .queryParam("delay", value)
                    .queryParam("unit", unit)
                    .get("/api/v1/delay");
            ensureOk(response, "Could NOT enable delays");
            log.info("Delays enabled");
        }

        @Override
        public void disable() {
            Response response = spec(machine)
                    .get("/api/v1/delay");
            ensureOk(response, "Could NOT revert delays.");
            log.info("Delays disabled");
        }
    }
}
