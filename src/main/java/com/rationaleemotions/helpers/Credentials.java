package com.rationaleemotions.helpers;

public record Credentials(String username, String password) {
}
