package com.rationaleemotions.helpers;

import com.rationaleemotions.pojos.JobScheduleLedger;
import org.knowm.yank.Yank;

import java.util.Optional;
import java.util.Properties;

@SuppressWarnings("unused")
public class DBHelper {

    private final String db;

    public DBHelper(JdbcUrl jdbcUrl, Credentials credentials) {
        DataSource ds = new DataSource(jdbcUrl, credentials);
        db = jdbcUrl.database();
        Yank.setupConnectionPool(db, ds.asProperties());
        Runtime.getRuntime().addShutdownHook(new Thread(Yank::releaseAllConnectionPools));
    }

    public Optional<JobScheduleLedger> latestEntryInLedger() {
        JobScheduleLedger result = Yank.queryBean(db, "SELECT * FROM job_schedule_ledger jsl ORDER BY creation_date_time  desc LIMIT 1",
                JobScheduleLedger.class, new Object[0]);
        return Optional.ofNullable(result);
    }

    public Optional<JobScheduleLedger> getLedgerEntryForJobId(long jobId) {
        JobScheduleLedger result = Yank.queryBean(db, "SELECT * FROM job_schedule_ledger where id = ?",
                JobScheduleLedger.class, new Object[] {jobId});
        return Optional.ofNullable(result);
    }

    public void purgeJobIdFromLedger(long jobId) {
        int result = Yank.execute(db, "delete from job_scheduler_ledge where job_id = ?", new Object[]{jobId});
        if (result == 0) {
            throw new IllegalArgumentException("No data found for jobId " + jobId);
        }
    }

    public void purgeIngestionLedger() {
        Yank.execute(db, "delete from  job_schedule_ledger", new Object[0]);
    }

    public void purgeAllIngestionData() {
        Yank.execute(db, "delete from  third_party_table", new Object[0]);
    }

    public record DataSource(JdbcUrl jdbcUrl, Credentials credentials) {

        public Properties asProperties() {
            Properties props = new Properties();
            props.put("username", credentials.username());
            props.put("password", credentials.password());
            props.put("jdbcUrl", jdbcUrl.get());
            return props;
        }

    }
}
