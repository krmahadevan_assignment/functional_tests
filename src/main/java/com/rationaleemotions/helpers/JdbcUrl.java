package com.rationaleemotions.helpers;

import com.rationaleemotions.pojos.Host;

public record JdbcUrl(Host machine, String database) {

    public String get() {
        return String.format("jdbc:postgresql://%s:%d/%s", machine.host(), machine.port(), database);
    }
}
