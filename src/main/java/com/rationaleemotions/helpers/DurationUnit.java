package com.rationaleemotions.helpers;

public enum DurationUnit {

    MILLIS, SECONDS
}
