package com.rationaleemotions.helpers;

import com.rationaleemotions.pojos.Host;
import com.rationaleemotions.pojos.NodeStatus;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.rationaleemotions.helpers.Utils.ensureOk;

public record WorkerHelper(Host machine) {

    private static final Logger log = LoggerFactory.getLogger(WorkerHelper.class);

    public void registerWorker() {
        Response response = Utils.spec(machine).get("/v1/admin/register");
        ensureOk(response, "Manual registration of Worker to Manager failed");
        log.info("Manual registration of Worker {} to Manager succeeded", machine);
    }

    public boolean free() {
        Response response = Utils.spec(machine).get("/v1/admin/is-free");
        ensureOk(response, "Failed to determine if worker was free");
        boolean value = response.body().as(NodeStatus.class).free();
        log.info("Node {} is free? {}", machine, value);
        return value;
    }

    public void markFree() {
        Response response = Utils.spec(machine).post("/v1/admin/mark-free");
        ensureOk(response, "Failed to mark worker free");
        log.info("Manually marked worker {} as free", machine);
    }
}
