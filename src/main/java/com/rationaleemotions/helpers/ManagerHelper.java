package com.rationaleemotions.helpers;

import com.rationaleemotions.pojos.Host;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.util.Optional;

import static com.rationaleemotions.helpers.Utils.ensureOk;
import static com.rationaleemotions.helpers.Utils.spec;

@Slf4j
public record ManagerHelper(Host machine) {

    public void generateRecords(int count) {
        Response response = spec(machine).get("/v1/admin/generate/{count}", count);
        ensureOk(response, "Could NOT generate test data");
        Records records = response.body().as(Records.class);
        log.info("Generated {} as test data", records);
    }

    public Optional<JobResponse> scheduleIngestion() {
        Response response = spec(machine).post(URI.create("/v1/api/schedule"));
        try {
            JobResponse result = response.body().as(JobResponse.class);
            log.info("Scheduled ingestion {}", result);
            return Optional.of(result);
        } catch (Exception e) {
            log.warn("Failed to schedule ingestion. Status {}", response.statusLine(), e);
        }
        return Optional.empty();
    }

    public boolean isNodePoolEmpty() {
        Response response = spec(machine).get("/v1/admin/node-pool-empty");
        ensureOk(response, "Could not determine if Node pool is empty");
        NodePoolStatus status = response.body().as(NodePoolStatus.class);
        log.info("Is node pool empty {}", status);
        return status.empty();
    }

    public record Records(long from, long to) {
    }
    public record NodePoolStatus(boolean empty) {
    }

    public record JobResponse(long jobId, String reason, boolean isError) {
        public JobResponse(long jobId, String reason) {
            this(jobId, reason, false);
        }
    }
}
