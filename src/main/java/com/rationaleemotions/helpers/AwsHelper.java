package com.rationaleemotions.helpers;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
public class AwsHelper {

    private final AwsClientBuilder.EndpointConfiguration endpointConfig;
    private final AWSCredentials credentials;
    private final AmazonS3 awsClient;

    public AwsHelper(AwsClientBuilder.EndpointConfiguration endpointConfig, AWSCredentials credentials) {
        this.endpointConfig = endpointConfig;
        this.credentials = credentials;
        this.awsClient = s3Client();
    }

    public String fileContents(String bucketName, String keyName) {
        try {
            S3Object object = awsClient.getObject(bucketName, keyName);
            S3ObjectInputStream objectInputStream = object.getObjectContent();

            StringBuilder contentBuilder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(objectInputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                contentBuilder.append(line);
            }

            // Close the streams
            objectInputStream.close();
            reader.close();

            return contentBuilder.toString();

        } catch (SdkClientException | IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private AmazonS3 s3Client() {
        return AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(endpointConfig)
                .withCredentials(getCredentialsProvider(credentials))
                .build();
    }

    private AWSStaticCredentialsProvider getCredentialsProvider(AWSCredentials credentials) {
        return new AWSStaticCredentialsProvider(credentials);
    }
}
