package com.rationaleemotions.pojos;

public record NodeStatus(boolean free) {
}
