package com.rationaleemotions.pojos;

public record Host(String host, int port) {
}
