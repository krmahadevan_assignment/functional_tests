package com.rationaleemotions.pojos;

import java.util.Arrays;
import java.util.Optional;

public enum JobStatus {

    Aborted,
    Submitted,
    Processing,
    Success,
    Failed;

    public static JobStatus fromString(String text) {
        String status = Optional.ofNullable(text).orElse("").trim().toUpperCase();
        return Arrays.stream(values())
                .filter(it -> it.name().toUpperCase().equals(status))
                .findFirst()
                .orElse(Aborted);
    }

    public static boolean isCompleted(JobStatus jobStatus) {
        return jobStatus == Failed || jobStatus == Success;
    }

    public static boolean isInFlight(JobStatus jobStatus) {
        return jobStatus == Submitted || jobStatus == Processing;
    }
}
