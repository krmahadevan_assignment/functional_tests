package com.rationaleemotions.pojos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.knowm.yank.annotations.Column;

import java.sql.Timestamp;
import java.time.LocalDateTime;

//@Accessors(chain = true, fluent = true)
@Getter
@Setter
@ToString
public class JobScheduleLedger {
    @Column("id")
    private Long id;

    @Column("node_id")
    private String nodeId;

    @Column("starting_key")
    private long startingKey;

    @Column("ending_key")
    private long endingKey;

    @Column("job_status")
    private String jobStatus;

    @Column("records")
    private int records;

    @Column("file_location")
    private String fileLocation;

    @Column("creation_date_time")
    private Timestamp creationDateTime;

    @Column("last_update_date_time")
    private Timestamp lastUpdateDateTime;

    @Column("reason")
    private String reason;

}
