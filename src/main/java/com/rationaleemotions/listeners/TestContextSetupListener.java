package com.rationaleemotions.listeners;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.rationaleemotions.helpers.Credentials;
import com.rationaleemotions.pojos.Host;
import lombok.extern.slf4j.Slf4j;
import org.testng.*;
import org.testng.internal.collections.Pair;

import java.util.Optional;

@Slf4j
public class TestContextSetupListener implements ISuiteListener, IInvokedMethodListener {

    public static final String MANAGER_PROXY = "ManagerProxy";
    public static final String WORKER_PROXY = "WorkerProxy";
    public static final String WORKER_HOST = "WorkerHost";
    public static final String MANAGER_HOST = "ManagerHost";

    public static final String DB_HOST = "DBHost";
    public static final String DB_CREDENTIALS = "DBCredentials";
    public static final String DB_NAME = "DBName";
    public static final String S3_ENDPOINT = "S3Endpoint";
    public static final String AWS_CREDENTIALS = "AWSCredentials";
    public static final String BUCKET_NAME = "BucketName";
    private static final String DELIMITER = "-".repeat(50);

    @Override
    public void onStart(ISuite suite) {
        Host mgrProxy = hostInfo(
                suite, Pair.of("managerProxyHost", "localhost"),
                Pair.of("managerProxyPort", "6666")
        );
        Host mgr = hostInfo(
                suite,
                Pair.of("managerHost", "localhost"),
                Pair.of("managerPort", "4444")
        );
        Host wrkProxy = hostInfo(
                suite,
                Pair.of("workerProxyHost", "localhost"),
                Pair.of("workerProxyPort", "6667"));
        Host wrk = hostInfo(
                suite,
                Pair.of("workerHost", "localhost"),
                Pair.of("workerPort", "5555")
        );

        Host dbHost = hostInfo(
                suite,
                Pair.of("dbHost", "localhost"),
                Pair.of("dbPort", "5432")
        );

        Credentials credentials = credentials(suite,
                Pair.of("username", "postgres"),
                Pair.of("password", "postgres")
        );

        AwsClientBuilder.EndpointConfiguration endpointConfiguration = endpointConfiguration(
                suite,
                Pair.of("s3Endpoint", "http://127.0.0.1:4566"),
                Pair.of("region", "us-east-1")
        );

        AWSCredentials awsCredentials = basicCredentials(
                suite,
                Pair.of("accessKey", "test"),
                Pair.of("secretKey", "test")
        );

        suite.setAttribute(MANAGER_PROXY, mgrProxy);
        suite.setAttribute(WORKER_PROXY, wrkProxy);
        suite.setAttribute(WORKER_HOST, wrk);
        suite.setAttribute(MANAGER_HOST, mgr);
        suite.setAttribute(DB_HOST, dbHost);
        suite.setAttribute(DB_CREDENTIALS, credentials);
        suite.setAttribute(DB_NAME, get(suite, Pair.of("dbName", "assessment_db")));
        suite.setAttribute(S3_ENDPOINT, endpointConfiguration);
        suite.setAttribute(AWS_CREDENTIALS, awsCredentials);
        suite.setAttribute(BUCKET_NAME, get(suite, Pair.of("bucketName", "assignment")));
        log.info("Test Context Setup Complete");
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isConfigurationMethod()){
            return;
        }
        log.info(DELIMITER);
        log.info("Beginning test-case: {}", method.getTestMethod().getMethodName());
        log.info(DELIMITER);
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isConfigurationMethod()) {
            return;
        }
        log.info(DELIMITER);
        log.info("Completed test-case: {} Result: {}", method.getTestMethod().getMethodName(), result(testResult));
        log.info(DELIMITER);
    }

    private Host hostInfo(ISuite suite, Pair<String, String> hostInfo, Pair<String, String> portInfo) {
        String hostname = get(suite, hostInfo);
        int port = Integer.parseInt(get(suite, portInfo));
        return new Host(hostname, port);
    }

    private Credentials credentials(ISuite suite, Pair<String, String> userInfo, Pair<String, String> pwdInfo) {
        String username = get(suite, userInfo);
        String password = get(suite, pwdInfo);
        return new Credentials(username, password);
    }

    private AwsClientBuilder.EndpointConfiguration endpointConfiguration(
            ISuite suite, Pair<String, String> endPointInfo, Pair<String, String> regionInfo) {
        String endpoint = get(suite, endPointInfo);
        String region = get(suite, regionInfo);
        return new AwsClientBuilder.EndpointConfiguration(endpoint, region);
    }

    private BasicAWSCredentials basicCredentials(ISuite suite,
                                                 Pair<String, String> accessKeyInfo, Pair<String, String> secretKeyInfo) {
        String accessKey = get(suite, accessKeyInfo);
        String secretKey = get(suite, secretKeyInfo);
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    private static String get(ISuite suite, Pair<String, String> data) {
        return Optional.ofNullable(suite.getParameter(data.first())).orElse(data.second());
    }

    private static String result(ITestResult itr) {
        if (itr.getStatus() == ITestResult.SUCCESS) {
            return "PASSED";
        }
        if (itr.getStatus() == ITestResult.FAILURE) {
            return "FAILED";
        }
        if (itr.getStatus() == ITestResult.SKIP) {
            return "SKIPPED";
        }
        return "UNKNOWN-RESULT";
    }
}
