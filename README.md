# Data Ingestion functional tests

This repository hosts a bunch of basic functional tests, that aim at testing a data ingestion pipeline.

## Pre-requisites

* Maven
* JDK17
* Docker (builds generate a docker image as well)
* The environment to be available as explained [here] (https://gitlab.com/krmahadevan_assignment/environment)

## Running the tests

To execute the tests, run:

```shell
mvn clean test
```
